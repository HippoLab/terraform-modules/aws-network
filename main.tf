terraform {
  required_version = ">=0.13.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=3.0.0"
    }
  }

}

data "aws_availability_zones" "available" {}

locals {
  subnets = flatten([
    for subnet in var.subnets : [
      for az in data.aws_availability_zones.available.names : [
        merge(subnet, { availability_zone = az })
      ]
    ]
  ])
  common_tags = {
    ManagedBy = "terraform"
  }
}
