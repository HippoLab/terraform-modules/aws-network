Changelog
=========

## 0.0.3
- Prevent changes to existing subnets when adding new ones. Loop over subnets before switching to availability zones

## 0.0.2
- Adding outputs for VPC and subnet CIDR
- Renaming "vpc_cidr" output to "vpc_ipv4_cidr"
- Fixing typos in README

## 0.0.1
First release
