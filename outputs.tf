output "vpc_id" {
  description = "VPC ID"
  value       = aws_vpc.vpc.id
}

output "vpc_ipv4_cidr" {
  description = "VPC IPv4 CIDR e.g. 10.0.0.0/16"
  value       = aws_vpc.vpc.cidr_block
}

output "vpc_ipv6_cidr" {
  description = "VPC IPv6 CIDR e.g. FE80::0/64"
  value       = aws_vpc.vpc.cidr_block
}

output "subnet_ids" {
  description = "Produces a map { 'subnet_name': ['az1_id', 'az2_id', 'az3_id'] }"
  value = {
    for subnet in var.subnets :
    subnet["name"] => [
      for az in data.aws_availability_zones.available.names : aws_subnet.subnet["${subnet["name"]}-${az}"]["id"]
    ]
  }
}

output "subnet_ipv4_cidr" {
  description = "Produces a map { 'subnet_name': ['az1_cidr', 'az2_cidr', 'az3_cidr'] }"
  value = {
    for subnet in var.subnets :
    subnet["name"] => [
      for az in data.aws_availability_zones.available.names : aws_subnet.subnet["${subnet["name"]}-${az}"]["cidr_block"]
  ]
  }
}

output "subnet_ipv6_cidr" {
  description = "Produces a map { 'subnet_name': ['az1_cidr', 'az2_cidr', 'az3_cidr'] }"
  value = {
    for subnet in var.subnets :
    subnet["name"] => [
      for az in data.aws_availability_zones.available.names : aws_subnet.subnet["${subnet["name"]}-${az}"]["ipv6_cidr_block"]
  ]
  }
}

output "route_table_ids" {
  description = "Produces a map { 'subnet_name': 'route_table_id'] }"
  value = {
    for subnet in var.subnets :
    subnet["name"] => aws_route_table.table[subnet["name"]]["id"]
  }
}
